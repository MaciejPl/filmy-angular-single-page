import { Injectable } from '@angular/core';
import { Film } from './modele/film';

@Injectable({
  providedIn: 'root'
})
export class FilmyService {

  private filmy: Film[] = [
    {id: 0, tytul: 'Titanic', opis: 'Rok 1912, brytyjski ststek Titanic w...', rok: 1997},
    {id: 1, tytul: 'Terminator', opis: 'Elektroniczny morderca zostaje wysłany...', rok: 1984},
    {id: 2, tytul: 'Avatar', opis: 'Jake, sparaliżowany były komandos, zo...', rok: 2009},
  ]

  constructor() { }

  wszystkieFilmy(): Film[] {
    return this.filmy;
  }
  getFilm(id: number): Film{
    return this.filmy[id];
  }
}
